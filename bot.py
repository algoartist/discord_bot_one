import discord
from discord.ext import commands

# Add token from dashbooard
TOKEN = ''

client = commands.Bot(command_prefix='abc')

#detects when the bot is ready
#logged in, collects all data from discord...
@client.event
#async can be run at any point
async def on_ready():
  print('Bot is ready')


@client.event
async def on_message(message):
  author = message.author
  content = message.content
  print(f'{author}: {content}')

@client.event
async def on_message_delete(message):
  author = message.author
  content = message.content
  channel = message.channel
  msg = f'{author} {content}'
  await channel.send(msg)
client.run(TOKEN)
